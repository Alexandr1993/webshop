import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;


public class BaseTest {
    protected static final String BASE_URL = "https://demowebshop.tricentis.com/";
    private void setup() {
        WebDriverManager.chromedriver().setup();
        Configuration.driverManagerEnabled = true;
        Configuration.browser = "chrome";
        Configuration.pageLoadTimeout = 100000;
        Configuration.browserSize = "1920x1080";
    }

    @BeforeSuite
    public void init() {
        setup();
    }

    @AfterSuite
    public void tearDown() {
        Selenide.closeWebDriver();
    }

}
