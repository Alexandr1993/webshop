import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class ListenerTest implements ITestListener {


    @Override
    public void onTestStart(ITestResult result) {
        System.out.println("Cтарт теста " + result.getName());
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        System.out.println("Тест " + result.getName() + " успешно пройден" );
    }

    @Override
    public void onTestFailure(ITestResult result) {
        System.out.println("Тест " + result.getName() + " не пройден" );
    }

    @Override
    public void onTestSkipped(ITestResult result) {

    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {

    }

    @Override
    public void onTestFailedWithTimeout(ITestResult result) {

    }

    @Override
    public void onStart(ITestContext context) {
        System.out.println("Старт тестирования : " +  context.getName());
    }

    @Override
    public void onFinish(ITestContext context) {
        System.out.println("Tecтирования " +  context.getName() + " завершено");
    }
}


