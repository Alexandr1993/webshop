import org.testng.annotations.Test;
import page.IndexPage;

public class IndexTest extends BaseTest {

    @Test(description = "Прокрутка страницы вниз")
    public void checkScrolling() {
        new IndexPage(BASE_URL).scrolling();
    }

}
