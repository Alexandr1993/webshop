package page;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.*;

public class IndexPage {

    private static final SelenideElement buttonLogin = $x("//a[@href=\"/login\"]");
    private static final SelenideElement buttonRegister = $x("//a[@href = \"/register\"]");
    private static final SelenideElement hrefGoogle = $(By.cssSelector("[class=\"google-plus\"]"));
    /** Метод для открытия сайта*/
    public IndexPage (String url) {
        open(url);
    }
    /** Метод для перехода на страницу авторизации*/
    public LoginPage clickLogin() {
        buttonLogin.click();
        return page(LoginPage.class);
    }

    public RegistrationPage clickRegister() {
        buttonRegister.click();
        return page(RegistrationPage.class);
    }

    /** Прокрутка страницы */
    public IndexPage scrolling() {
        hrefGoogle.scrollIntoView(true);
        return this;
    }







}
