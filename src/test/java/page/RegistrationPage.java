package page;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;

public class RegistrationPage {

    private static final SelenideElement gender = $(By.name("Gender"));
    private static final SelenideElement firstName = $x("//input[@name=\"FirstName\"]");
    private static final SelenideElement lastName = $x("//input[@name=\"LastName\"]");
    private static final SelenideElement email = $(By.cssSelector("#Email"));
    private static final SelenideElement password = $(By.cssSelector("#Password"));
    private static final SelenideElement confirmPassword = $(By.cssSelector("#ConfirmPassword"));

    private final SelenideElement buttonRegister = $(By.cssSelector("#register-button"));
    private final SelenideElement textErrorEmail = $(By.cssSelector(".validation-summary-errors"));
    private final SelenideElement textErrorLengthPassword = $x("//span[@for=\"Password\"]");
    private final SelenideElement textErrorConfirmPassword = $x("//span[@for=\"ConfirmPassword\"]");


    /**Метод для выбора пола*/
    public RegistrationPage clickGender(String sex) {
        gender.selectRadio(sex).shouldBe(Condition.value(sex));
        return this;
    }

    /**Метод для ввода в полях регистрации*/
    public RegistrationPage enterDate(SelenideElement obj, String text) {
        obj.setValue(text).shouldBe(Condition.value(text)).pressEnter();
        return this;
    }

    /**Метод для нажатия на кнопку зарегистрироваться*/
    public RegistrationPage clickButtonRegister() {
        buttonRegister.click();
        return this;
    }
    public String getErrorEmail() {
        return textErrorEmail.getText();
    }

    public String getErrorLengthPassword() {
        return textErrorLengthPassword.getText();
    }

    public String getErrorConfirmPassword() {
        return textErrorConfirmPassword.getText();
    }


    public static SelenideElement getFirstName() {
        return firstName;
    }

    public static SelenideElement getLastName() {
        return lastName;
    }

    public static SelenideElement getEmail() {
        return email;
    }

    public static SelenideElement getPassword() {
        return password;
    }

    public static SelenideElement getConfirmPassword() {
        return confirmPassword;
    }



}
