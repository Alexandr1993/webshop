package page;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;

public class LoginPage {

    private static final SelenideElement entryFildEmail = $x("//input[contains(@class, \"email\")]");
    private static final SelenideElement entryFildPassword = $x("//input[@class = \"password\"]");
    private static final SelenideElement incorrectEmail = $x("//span[@for = \"Email\"]");
    private static final SelenideElement errorPasswordEmail  = $x("//div[@class = \"validation-summary-errors\"]");

    /** Метод для ввода в поля email или password*/
    public LoginPage enterDate(String date, SelenideElement obj) {
        obj.setValue(date).shouldBe(Condition.value(date));
        return this;
    }

    /** Получение доступа к полю email*/
    public static SelenideElement getEntryFildEmail() {
        return entryFildEmail;
    }
    /** Получение доступа к полю password*/
    public static SelenideElement getEntryFildPassword() {
        return entryFildPassword;
    }

    /** Метод для получения текста ошибки при некорректном вводе email*/
    public String getErrorLogin() {
        return incorrectEmail.getText();
    }

    /** Метод для получения текста ошибки, если аккаунта не существует*/
    public String getErrorLoginPassword() {
        return errorPasswordEmail.getText();
    }

    /** Метод для подтверждения email*/
    public LoginPage confirmEmail() {
        entryFildEmail.pressEnter();
        return this;
    }



}
