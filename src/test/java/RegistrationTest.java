import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import page.IndexPage;
import page.RegistrationPage;

@Listeners(ListenerTest.class)
public class RegistrationTest extends BaseTest{
    private final String TEXT_ERROR_EMAIL = "The specified email already exists";
    private final String TEXT_ERROR_LENGTH_PASSWORD = "The password should have at least 6 characters.";
    private final String TEXT_ERROR_CONFIRM_PASSWORD = "The password and confirmation password do not match.";

    @DataProvider
    public Object[][] genderData() {
        return new Object[][] {
                {"M", "Alexandr", "Solovyov", "res@gmail.com", "123456", "123456"},
                {"F", "Darya", "Solovyova", "dar@gmail.com", "623456", "623456"}
        };
    }

    @Test(description = "Проверка ввода в поля регистрации и ошибки несуществущей почты", dataProvider = "genderData")
    public void checkClickRegister(String gender, String first, String last,
                                   String email, String password, String confirmPassword) {
        Assert.assertEquals(new IndexPage(BASE_URL)
                .clickRegister().clickGender(gender)
                .enterDate(RegistrationPage.getFirstName(), first)
                .enterDate(RegistrationPage.getLastName(), last)
                .enterDate(RegistrationPage.getEmail(), email)
                .enterDate(RegistrationPage.getPassword(), password)
                .enterDate(RegistrationPage.getConfirmPassword(), confirmPassword)
                .clickButtonRegister()
                .getErrorEmail(), TEXT_ERROR_EMAIL);
    }

    @Test(description = "Проверка ошибки при вводе менее 6 символов в поле пароль",
            dependsOnMethods = {"checkClickRegister"})
    public void checkErrorLengthPassword() {
        Assert.assertEquals(new RegistrationPage()
                .enterDate(RegistrationPage.getPassword(), "12345")
                .getErrorLengthPassword(), TEXT_ERROR_LENGTH_PASSWORD);
    }

    @Test(description = "Проверка ошибки при вводе менее 6 символов в поле пароль",
            dependsOnMethods = {"checkClickRegister"})
    public void checkErrorConfirmPassword() {
        Assert.assertEquals(new RegistrationPage()
                .enterDate(RegistrationPage.getPassword(), "123456")
                .enterDate(RegistrationPage.getConfirmPassword(), "123450")
                .getErrorConfirmPassword(), TEXT_ERROR_CONFIRM_PASSWORD);
    }

}
