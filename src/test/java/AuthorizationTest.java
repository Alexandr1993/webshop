import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import page.IndexPage;
import page.LoginPage;

@Listeners(ListenerTest.class)
public class AuthorizationTest extends BaseTest {

    public static final String MESSAGE_ERROR_EMAIL = "Please enter a valid email address.";
    public static final String MESSAGE_ERROR_ACCOUNT = "" +
            "Login was unsuccessful. " +
            "Please correct the errors and try again.\n" +
            "No customer account found";

    @DataProvider
    public Object[][] passwordEmail() {

        return new Object[][] {
                {"entry@gmail.com", "12345678"},
                {"18solovjov@yandex.ru", "elenasolo18568"},
                {"18solovjov@rambler.ru", "1"}
        };
    }

    @DataProvider
    public Object[][] errorLogin() {
        return new Object[][] {
                {"asdawe"},
                {"123214"},
                {"18solovjov@rambler"}
        };
    }

    @Test(description = "Проверка ввода пароля и почты", dataProvider = "passwordEmail")
    public void checkLoginPassword(String email, String password) {
        new IndexPage(BASE_URL)
                .clickLogin()
                .enterDate(password, LoginPage.getEntryFildPassword())
                .enterDate(email, LoginPage.getEntryFildEmail());
    }

    @Test(description = "Проверка корректности введеной почты", priority = 2, dataProvider = "errorLogin")
    public void checkErrorLogin(String email) {
        Assert.assertEquals(new LoginPage()
                .enterDate(email, LoginPage.getEntryFildEmail())
                .confirmEmail()
                .getErrorLogin(), MESSAGE_ERROR_EMAIL);
    }

    @Test(description = "Проверка не существующего аккаунта", dependsOnMethods = {"checkLoginPassword()"},
            priority = 1)
    public void checkErrorAccount() {
        Assert.assertEquals(new LoginPage()
                        .confirmEmail()
                        .getErrorLoginPassword(), MESSAGE_ERROR_ACCOUNT);
    }
}
